Description: <short summary of the patch>
 TODO: Put a short summary on the line above and replace this paragraph
 with a longer explanation of this change. Complete the meta-information
 with other relevant fields (see below for details). To make it easier, the
 information below has been extracted from the changelog. Adjust it or drop
 it.
 .
 ncbi-entrez-direct (12.0.20190816+ds-1) unstable; urgency=medium
 .
   [ Aaron M. Ucko ]
   * New upstream release.  (Not fully caught up, but uploading anyway to
     address an FTBFS bug that turned up.)
   * debian/control:
     - Build-depend on github-gedex-inflector-dev.
     - Declare a nominal XS-Go-Import-Path to placate dh-golang.
   * debian/examples: Install BioThings support as examples for now: bt-*
     (including one data file currently expected to live alongside scripts)
     and xplore.
   * debian/man/{download-ncbi-data,esample,exclude-uid-lists,index-bioc,j2x,
     pm-collect,xml2tbl}.1: Document new commands.
   * debian/man/{download-{pubmed,sequence},efetch,efilter,esearch,fetch-pubmed,
     ftp-{cp,ls},index-pubmed,intersect-uid-lists,nquire,pm-index,rchive,
     transmute,xtract}.1: Update for new release.
   * debian/man/download-sequence.1: Fix copy-and-paste error (wrong name
     left in DESCRIPTION).
   * debian/rules:
     - Belatedly use FIX_PERL_SHEBANG for bin/edirect.
     - Factor out GH and GL macros for github.com and golang.org respectively.
     - Factor out a pattern rule for building Go executables (and a
       supporting rule for the gounidecode compatibility symlink).
     - Add $(GH)/gedex/inflector to GOLIBSRC_ (for xtract).
     - Account for new download script download-ncbi-data; as-is scripts
       esample, exclude-uid-lists, index-bioc, pm-collect, and xml2tbl; and Go
       executable j2x (which does not use common.go).
     - Set GOCACHE to avoid needing a writable (or simply existent!) HOME;
       wipe the cache in override_dh_auto_clean.  (Closes: 947995.)
   * .gitignore: Ignore the new go-cache tree.
 .
   [ Andreas Tille ]
   * d/watch: simplify + version=4
   * Standards-Version: 4.5.0 (routine-update)
   * debhelper-compat 12 (routine-update)
Author: Aaron M. Ucko <ucko@debian.org>
Bug-Debian: https://bugs.debian.org/947995

---
The information above should follow the Patch Tagging Guidelines, please
checkout http://dep.debian.net/deps/dep3/ to learn about the format. Here
are templates for supplementary fields that you might want to add:

Origin: <vendor|upstream|other>, <url of original patch>
Bug: <url in upstream bugtracker>
Bug-Debian: https://bugs.debian.org/<bugnumber>
Bug-Ubuntu: https://launchpad.net/bugs/<bugnumber>
Forwarded: <no|not-needed|url proving that it has been forwarded>
Reviewed-By: <name and email of someone who approved the patch>
Last-Update: 2020-02-03

--- /dev/null
+++ ncbi-entrez-direct-12.0.20190816+ds/.gitignore
@@ -0,0 +1,4 @@
+.pc
+bin
+go-build
+obj-*/
