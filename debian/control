Source: ncbi-entrez-direct
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Aaron M. Ucko <ucko@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               dh-python,
               golang-any,
               golang-github-dataence-porter2-dev,
               golang-github-fatih-color-dev,
               golang-github-gedex-inflector-dev,
               golang-github-gin-gonic-gin-dev,
               golang-github-klauspost-cpuid-dev,
               golang-github-klauspost-pgzip-dev,
               golang-github-rainycape-unidecode-dev,
               golang-github-shirou-gopsutil-dev,
               golang-golang-x-net-dev,
               golang-golang-x-text-dev,
               python3
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/ncbi-entrez-direct
Vcs-Git: https://salsa.debian.org/med-team/ncbi-entrez-direct.git
Homepage: http://www.ncbi.nlm.nih.gov/books/NBK179288
Rules-Requires-Root: no
XS-Go-Import-Path: ncbi.nlm.nih.gov/edirect

Package: ncbi-entrez-direct
Architecture: any
Multi-Arch: foreign
Depends: curl | wget,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: ${python3:Depends}
Suggests: curl,
          libxml-simple-perl,
          libxml2-utils,
          unzip,
          ${perl:Depends}
Built-Using: ${misc:Built-Using}
Description: NCBI Entrez utilities on the command line
 Entrez Direct (EDirect) is an advanced method for accessing NCBI's set
 of interconnected databases (publication, sequence, structure, gene,
 variation, expression, etc.) from a terminal window or script.
 Functions take search terms from command-line arguments.  Individual
 operations are combined to build multi-step queries.  Record retrieval
 and formatting normally complete the process.
 .
 EDirect also provides an argument-driven function that simplifies the
 extraction of data from document summaries or other results that are
 returned in structured XML format.  This can eliminate the need for
 writing custom software to answer ad hoc questions.  Queries can move
 seamlessly between EDirect commands and UNIX utilities or scripts to
 perform actions that cannot be accomplished entirely within Entrez.
