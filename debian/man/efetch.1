.TH EFETCH 1 2023-02-12 NCBI "NCBI Entrez Direct User's Manual"
.SH NAME
efetch, esummary \- retrieve results from an NCBI Entrez search
.SH SYNOPSIS
\fBefetch\fP
[\|\fB\-help\fP\|]
[\|\fB\-format\fP\ \fIfmt\fP\|]
[\|\fB\-mode\fP\ \fImode\fP\|]
[\|\fB\-style\fP\ \fIstyle\fP\|]
[\|\fB\-db\fP\ \fIname\fP\|]
[\|\fB\-id\fP\ \fIID\fP\|]
[\|\fB\-input\fP\ \fIfilename\fP\|]
[\|\fB\-seq_start\fP\ \fIN\fP\|]
[\|\fB\-seq_stop\fP\ \fIN\fP\|]
[\|\fB\-strand\fP\ \fIN\fP\|]
[\|\fB\-forward\fP\|]
[\|\fB\-revcomp\fP\|]
[\|\fB\-chr_start\fP\ \fIN\fP\|]
[\|\fB\-chr_stop\fP\ \fIN\fP\|]
[\|\fB\-complexity\fP\ \fIN\fP\|]
[\|\fB\-extend\fP\ \fIN\fP\|]
[\|\fB\-extrafeat\fP\ \fIN\fP\|]
[\|\fB\-showgaps\fP\|]
[\|\fB\-start\fP\ \fIN\fP\|]
[\|\fB\-stop\fP\ \fIN\fP\|]
[\|\fB\-raw\fP\|]
[\|\fB\-express\fP\|]
[\|\fB\-immediate\fP\|]
[\|\fB\-json\fP\|]

\fBesummary\fP
[\|\fB\-help\fP\|]
[\|\fB\-mode\fP\ \fImode\fP\|]
[\|\fB\-db\fP\ \fIname\fP\|]
[\|\fB\-id\fP\ \fIID\fP\|]
[\|\fB\-input\fP\ \fIfilename\fP\|]
[\|\fB\-raw\fP\|]
.SH DESCRIPTION
\fBefetch\fP and \fBesummary\fP retrieve results
from either an Entrez Direct pipeline
or an immediate lookup (via \fB\-db\fP and \fB\-id\fP or \fB\-input\fP).
\fBesummary\fP is equivalent to \fBefetch -format docsum\fP.

\fBefetch\fP is also the name of an AceDB tool
for consulting local sequence databases.
To resolve this ambiguity,
Debian systems with both AceDB tools and Entrez Direct installed
arrange for AceDB's executable to have the name \fBefetch.acedb\fP(1)
and for \fBefetch\fP to be a wrapper script
that examines its usage and proceeds to run whichever of
\fBefetch.acedb\fP(1) or \fBefetch.ncbi\fP
looks like a better fit.
.SH OPTIONS
.SS Format Selection
.TP
\fB\-format\fP\ \fIfmt\fP
Format of record or report.
(See \fB-help\fP output for examples.)
.TP
\fB\-mode\fP\ \fImode\fP
.BR text ,
.BR xml ,
.BR asn.1 ,
or
.BR json .
.TP
\fB\-style\fP\ \fIstyle\fP
\fBmaster\fP or \fBconwithfeat\fP.
.SS Direct Record Selection
.TP
\fB\-db\fP\ \fIname\fP
Entrez database name for immediate lookups.
.TP
\fB\-id\fP\ \fIID\fP
Unique identifier or accession for immediate lookups.
.TP
\fB\-input\fP\ \fIfilename\fP
Read identifer(s) from file instead of standard input.
.SS Sequence Range
.TP
\fB\-seq_start\fP\ \fIN\fP
First sequence position to retrieve (1\-based).
.TP
\fB\-seq_stop\fP\ \fIN\fP
Last sequence position to retrieve (1\-based).
.TP
\fB\-strand\fP\ \fIN\fP
Strand of DNA to retrieve:
\fB1\fP for forward (plus), \fB2\fP for reverse complement (minus).
.TP
\fB\-forward\fP
Shortcut for \fB\-strand 1\fP.
.TP
\fB\-revcomp\fP
Shortcut for \fB\-strand 2\fP.
.SS Gene Range
.TP
\fB\-chr_start\fP\ \fIN\fP
First sequence position to retrieve (0\-based).
.TP
\fB\-chr_stop\fP\ \fIN\fP
Last sequence position to retrieve (0\-based).
.SS Sequence Flags
.TP
\fB\-complexity\fP\ \fIN\fP
How much context to fetch:
.RS
.PD 0
.IP \fB0\fP
default
.IP \fB1\fP
Bioseq
.IP \fB3\fP
Nuc\-prot set
.PD
.RE
.TP
\fB\-extend\fP\ \fIN\fP
Extend sequence retrieval by \fIN\fP residues in both directions.
.TP
\fB\-extrafeat\fP\ \fIN\fP
Bit flag specifying extra features.
.TP
\fB\-showgaps\fP
Propagate component gaps.
.SS Subset Retrieval
.TP
\fB\-start\fP\ \fIN\fP
First record to fetch.
.TP
\fB\-stop\fP\ \fIN\fP
Last record to fetch.
.SS Miscellaneous
.TP
\fB\-raw\fP
Skip database\-specific XML modifications.
.TP
\fB\-express\fP
Direct sequence retrieval in groups of five.
.TP
\fB\-immediate\fP
Express mode on a single record at a time.
.TP
\fB\-json\fP
Convert adjusted XML output to JSON.
.TP
\fB\-help\fP
Print usage information, complete with examples of notable
.BR \-db / \-format / \-mode
combinations (in the case of \fBefetch\fP)
and of supplying accessions in the \fB\-id\fP field
(in the case of \fBesummary\fP).
.SH SEE ALSO
.BR combine\-uid\-lists (1),
.BR difference\-uid\-lists (1),
.BR ds2pme (1),
.BR efetch.acedb (1),
.BR esample (1),
.BR esearch (1),
.BR exclude\-uid\-lists (1),
.BR hgvs2spdi (1),
.BR intersect\-uid\-lists (1),
.BR nquire (1),
.BR pma2pme (1),
.BR rchive (1),
.BR transmute (1),
.BR xml2fsa (1),
.BR xml2tbl (1),
.BR xtract (1).
