package memory

import ("github.com/shirou/gopsutil/mem")

func TotalMemory() uint64 {
	v, err := mem.VirtualMemory()
	if err == nil {
		return v.Total
	} else {
		return 0
	}
}
